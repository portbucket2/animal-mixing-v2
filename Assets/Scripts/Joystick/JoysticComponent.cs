﻿using UnityEngine;
using UnityEngine.Events;
using com.faith.gameplay.service;


public abstract class JoysticComponent : BillyTheCloudBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        OnJoystickDownEvent += OnJoystickDown;
        OnJoystickEvent     += OnJoystick;
        OnJoystickUpEvent   += OnJoystickUp;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnDataLoaded()
    {
        base.OnDataLoaded();

    }

    protected override void OnLevelStarted(){

        base.OnLevelStarted();


        GlobalTouchController.Instance.EnableTouchController();

        GlobalTouchController.Instance.OnTouchDown  += OnTouchDown;
        GlobalTouchController.Instance.OnTouch      += OnTouch;
        GlobalTouchController.Instance.OnTouchUp    += OnTouchUp;
    }

    protected override void OnLevelEnded()
    {
        base.OnLevelEnded();

        GlobalTouchController.Instance.OnTouchDown  -= OnTouchDown;
        GlobalTouchController.Instance.OnTouch      -= OnTouch;
        GlobalTouchController.Instance.OnTouchUp    -= OnTouchUp;

        GlobalTouchController.Instance.DisableTouchController();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public UnityAction<Vector2> OnJoystickDownEvent;
    public UnityAction<Vector2> OnJoystickEvent;
    public UnityAction<Vector2> OnJoystickUpEvent;

    #endregion

    #region Protected Variables

    [SerializeField] protected Joystick _joystick;

    #endregion

    #region Configuretion

    private void OnTouchDown(Vector3 touchPosition, int touchIndex) {

        OnJoystickDownEvent?.Invoke(new Vector2(_joystick.Horizontal, _joystick.Vertical));
    }

    private void OnTouch(Vector3 touchPosition, int touchIndex)
    {
        OnJoystickEvent?.Invoke(new Vector2(_joystick.Horizontal, _joystick.Vertical));
    }

    private void OnTouchUp(Vector3 touchPosition, int touchIndex)
    {
        OnJoystickUpEvent?.Invoke(new Vector2(_joystick.Horizontal, _joystick.Vertical));
    }

    #endregion

    #region Abstract Method

    protected abstract void OnJoystickDown(Vector2 joystickValue);
    protected abstract void OnJoystick(Vector2 joystickValue);
    protected abstract void OnJoystickUp(Vector2 joystickValue);


    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
