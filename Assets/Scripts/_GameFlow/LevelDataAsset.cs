﻿using UnityEngine;
using UnityEngine.Events;
using com.faith.core;

[CreateAssetMenu(fileName = "LevelDataAsset", menuName = GlobalConstant.GAME_NAME + "/GameFlow/LevelDataAsset")]
public class LevelDataAsset : ScriptableObject
{

    #region Custom Variables


    [System.Serializable]
    public class LevelInformation
    {
        public SceneReference       levelScene;

        [Space(5.0f)]
        public IntReference         numberOfAI;
        public RangeReference       delayBetweenSpawn;
    }

    #endregion

    #region Public Variables

    public int GetLevelIndex { get { return _levelIndex.GetData(); } }
    public int GetIncrementalLevelIndex { get
        {
            if (_incrementalLevelIndex == null) _incrementalLevelIndex = new SavedData<int>(INCREMENTAL_LEVEL_INDEX, 0);
            return _incrementalLevelIndex.GetData() + 1;
        }
    }

    public int NumberOfLevel { get { return levelInformations.Length; } }

    [Header("Material   :   WorldBending Material")]
    public float effectiveDistance;
    public Material[] worldBendingMaterials;
    public Material[] inverseWorldBendingMaterials;

    [Header("Data   :   Shared")]
    public SharedData sharedGameData;

    [Header("Data   :   LevelData")]
    public float delayOfAppearingEndScene = 1f;
    public LevelInformation[]   levelInformations;

    #endregion

    #region Private Varaibles

    private const string INCREMENTAL_LEVEL_INDEX = "INCREMENTAL_LEVEL_INDEX";

    private bool            _isDataLoaded;
    private SavedData<int>  _levelIndex;
    private SavedData<int> _incrementalLevelIndex;
    private UnityAction     OnLevelDataLoaded;

    #endregion

    #region Configuretion

    private void OnLevelIndexLoaded(int _loadedDayIndex)
    {

        sharedGameData.levelIndex = _loadedDayIndex;
        if (!_isDataLoaded)
        {
            _isDataLoaded = true;
            OnLevelDataLoaded.Invoke();
        }
    }

    private bool IsValidLevelIndex(int dayIndex)
    {

        if (dayIndex >= 0 && dayIndex < NumberOfLevel)
        {

            return true;
        }

        return false;
    }

    private void SetEffectiveDistance()
    {
        foreach (Material worldBendingMaterial in worldBendingMaterials)
        {

            worldBendingMaterial.SetFloat("_EffectiveDistance", effectiveDistance);
        }

        foreach (Material inverseWorldBendingMaterial in inverseWorldBendingMaterials)
        {

            inverseWorldBendingMaterial.SetFloat("_EffectiveDistance", effectiveDistance);
        }
    }

    #endregion

    #region Public Callback

    public void Initialization(UnityAction OnLevelDataLoaded)
    {
        SetValueForWorldBendingMaterial(Vector2.zero);

        _isDataLoaded = false;
        this.OnLevelDataLoaded = OnLevelDataLoaded;

        sharedGameData          = new SharedData();
        _incrementalLevelIndex  = new SavedData<int>(INCREMENTAL_LEVEL_INDEX, 0);
        _levelIndex             = new SavedData<int>("LEVEL_INDEX", 0, OnLevelIndexLoaded);
        

        SetEffectiveDistance();

    }

    public LevelInformation GetLevelInformationForCurrentLevel() {

        return levelInformations[_levelIndex.GetData()];
    }


    public void SetValueForWorldBendingMaterial(Vector2 bendValue) {

        foreach (Material worldBendingMaterial in worldBendingMaterials) {

            worldBendingMaterial.SetFloat("HorizontalDisplacement", bendValue.x);
            worldBendingMaterial.SetFloat("VerticalDisplacement", bendValue.y);
        }

        foreach (Material inverseWorldBendingMaterial in inverseWorldBendingMaterials) {

            inverseWorldBendingMaterial.SetFloat("HorizontalDisplacement", -bendValue.x);
            inverseWorldBendingMaterial.SetFloat("VerticalDisplacement", -bendValue.y);
        }
    }

    public void GotToNextLevel()
    {

        int nextDay = _levelIndex.GetData() + 1;
        if (IsValidLevelIndex(nextDay))
        {
            _levelIndex.SetData(nextDay);
        }
        else
        {
            _levelIndex.SetData(0);
        }

        int currentData = _incrementalLevelIndex.GetData();
        _incrementalLevelIndex.SetData(currentData + 1);
    }


    #endregion
}
