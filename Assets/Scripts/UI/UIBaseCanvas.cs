﻿using UnityEngine;

public abstract class UIBaseCanvas : BillyTheCloudBehaviour
{
    #region Protected Variables

    [SerializeField] protected GameObject rootObject;

    #endregion

}
