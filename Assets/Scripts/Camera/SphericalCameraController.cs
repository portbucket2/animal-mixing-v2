﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.coreconsole;
using Cinemachine;

public class SphericalCameraController : BillyTheCloudBehaviour, IBatchedUpdateHandler
{
    
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        _transformReference = transform;
        _sphericalVirtualCameraTransform = _sphericalVirtualCamera.transform;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnSceneLoaded()
    {
        base.OnSceneLoaded();

        BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);
    }

    protected override void OnLevelEnded()
    {
        base.OnLevelEnded();

        CameraController.Instance.SwitchCamera(_sphericalVirtualCamera);
        BatchedUpdate.Instance.RegisterToBatchedUpdate(this,1);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Private Variables

    [SerializeField] private CinemachineVirtualCamera _sphericalVirtualCamera;

    [Space(2.5f)]
    [SerializeField] private Transform _lookAtTransform;
    [SerializeField, Range(0.01f,1f)] private float _lookAtVelocity = 0.01f;
    [SerializeField] private float _rotationSpeed;

    private Transform _transformReference;
    private Transform _sphericalVirtualCameraTransform;
    private Quaternion _lookRotation;
    private float _deltaTime;

    #endregion

    #region Public Callback

    public void OnBatchedUpdate()
    {
        _deltaTime = Time.deltaTime;

        _transformReference.Rotate(Vector3.up * _rotationSpeed * _deltaTime);

        _lookRotation = Quaternion.Slerp(
                _sphericalVirtualCameraTransform.rotation,
                Quaternion.LookRotation(_lookAtTransform.position - _sphericalVirtualCameraTransform.position),
                _lookAtVelocity
            );
        _sphericalVirtualCameraTransform.rotation = _lookRotation;
    }

    #endregion


    #endregion ALL SELF DECLEAR FUNCTIONS

}
