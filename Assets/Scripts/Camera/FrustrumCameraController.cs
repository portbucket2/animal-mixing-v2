﻿
using UnityEngine;
using com.faith.core;

public class FrustrumCameraController : MonoBehaviour, IBatchedUpdateHandler
{
    #region Private Variables

    [Header("Parameter  :   Default")]
    [SerializeField] private bool _startOnAwake = false;
    [SerializeField] private Transform _defaultTransformOfFollowingObject;

    [Header("Parameter  :   CoreSettings")]
    [SerializeField] private Transform _transformOfCameraContainer;
    [SerializeField] private Transform _transformOfCamera;
    [SerializeField] private Vector3 _positionOffset;

    [SerializeField, Range(0f, 1f)] private float _frustrumRange;
    [SerializeField, Range(0f, 1f)] private float _forwardVelocity;
    [SerializeField, Range(0f, 1f)] private float _angulerVelocity;


    private Transform _transformOfFollowingObject;
    private Vector3 _currentVelocity;
    private float _dotProduct;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (_startOnAwake) {
            Follow();
        }

    }

    #endregion

    #region Public Callback

    public void Follow(Transform followingObject = null) {

        _transformOfFollowingObject = followingObject == null ? _defaultTransformOfFollowingObject : followingObject;

        BatchedUpdate.Instance.RegisterToBatchedUpdate(this, 1);
    }

    public void Unfollow() {

        BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);
    }

    public void OnBatchedUpdate()
    {
        _dotProduct = Vector3.Dot(_transformOfCamera.forward, Vector3.Normalize(_transformOfFollowingObject.position - _transformOfCamera.position));
        if (_dotProduct <= (1f - _frustrumRange)) {

            Vector3 newPosition = Vector3.SmoothDamp(
                    _transformOfCameraContainer.position,
                    _transformOfFollowingObject.position + _positionOffset,
                    ref _currentVelocity,
                    0.1f,
                    _forwardVelocity * 100
                );
            _transformOfCameraContainer.position = newPosition;

            
        }

        //Quaternion newRotation = Quaternion.Slerp(
        //        _transformOfCamera.rotation,
        //        Quaternion.LookRotation(_transformOfFollowingObject.position - _transformOfCamera.position),
        //        _angulerVelocity
        //    );
        //_transformOfCamera.rotation = newRotation;
    }

    #endregion
}
