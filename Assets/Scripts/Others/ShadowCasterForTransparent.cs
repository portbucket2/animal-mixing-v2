﻿
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ShadowCasterForTransparent : MonoBehaviour
{
    private void Awake()
    {
        gameObject.GetComponent<MeshRenderer>().material.SetShaderPassEnabled("ShadowCaster", true);
    }
}
