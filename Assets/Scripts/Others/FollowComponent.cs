﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.coreconsole;

public class FollowComponent : MonoBehaviour, IBatchedUpdateHandler
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    private void Awake()
    {
        _transformReference = transform;

        if (_startFromAwake) {
            Follow();
        }
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public bool IsFollowing { get; private set; }

    #endregion

    #region Private Variables

    [SerializeField] private bool _startFromAwake = false;
    [SerializeField] private bool _rotateTowardsTarget;

    [Space(2.5f)]
    [SerializeField] private Transform  _defaultLookAtTarget = null;
    [SerializeField] private Vector3    _positionOffset = Vector3.zero;

    [Space(2.5f)]
    [SerializeField, Range(0f,1f)] private float _forwardVeclocity = 0.1f;
    [SerializeField, Range(0f,1f)] private float _angulerVelocity = 0.1f;

    private Transform _transformReference;
    private Transform _lookAtTarget;

    private Vector3 _lookAtTargetPosition;

    private Vector3 _position;
    private Vector3 _newPosition;

    private Quaternion _rotation;
    private Quaternion _newRotation;

    #endregion

    #region Public Callback

    public void OnBatchedUpdate()
    {
        _lookAtTargetPosition = _lookAtTarget.position;

        _position = _transformReference.position;
        _newPosition = Vector3.Lerp(
                _position,
                _lookAtTargetPosition + _positionOffset,
                _forwardVeclocity
            );
        _transformReference.position = _newPosition;


        if (_rotateTowardsTarget) {

            _rotation = _transformReference.rotation;
            _newRotation = Quaternion.Slerp(
                    _rotation,
                    Quaternion.LookRotation(_lookAtTargetPosition - _newPosition),
                    _angulerVelocity
                );
            _transformReference.rotation = _newRotation;
        }
        
    }

    public void Follow(Transform lookAtTarget = null) {

        _lookAtTarget = lookAtTarget == null ? _defaultLookAtTarget : lookAtTarget;

        if (!IsFollowing) {

            BatchedUpdate.Instance.RegisterToBatchedUpdate(this, 1);
            IsFollowing = true;
        }
    }

    public void Unfollow() {

        if (IsFollowing) {
            BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);
            IsFollowing = false;
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
