﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using com.faith.core;

public class PowerBar : MonoBehaviour, IBatchedUpdateHandler
{
    #region Custom Variables

    [System.Serializable]
    private enum IndicatorMode
    {
        OneGo,
        Rubberbend
    }

    [System.Serializable]
    private enum IndicatorResponse
    {
        Auto,
        Manual
    }

    #endregion



    #region Public Variables

    public UnityAction<float> OnPassingTheResultEvent;

    public float InterpolatedPositionOfIndicator { get; private set; }

    #endregion

    #region Variables
    [Header("Parameter  :   Indicator")]
    [SerializeField] private bool isFollowTheObject = false;
    [SerializeField] private Vector3 positionOffset = Vector3.zero;

    [Space(5.0f)]
    [SerializeField] private IndicatorResponse indicatorResponse;
    [SerializeField] private IndicatorMode indicatorMode;
    [SerializeField] private RectTransform parentOfIndicator;
    [SerializeField] private RectTransform indicator;
    [SerializeField, Range(0.1f, 10)] private float indicatorVelocity = 1;

    [Header("Parameter  :   (Visual) PerformanceBar")]
    [SerializeField] private Transform parentOfPerformanceBar;
    [SerializeField] private Image fillerImage;
    [Space(5.0f)]
    [SerializeField, Range(0.1f, 0.5f)] private float colorBlendAreaOnLesserThanAccurate = 0.1f;
    [SerializeField, Range(0.1f, 0.5f)] private float colorBlendAreaOnGreaterThanAccurate = 0.1f;
    [SerializeField] private Color colorLesserThanAccurate = Color.yellow;
    [SerializeField] private Color colorForAccurate = Color.green;
    [SerializeField] private Color colorGreaterThanAccurate = Color.red;

    [Space(5.0f)]
    [SerializeField, Range(0.001f, 1f)] private float minLevelOfAcceptancy = 0.1f;
    [SerializeField, Range(0.001f, 1f)] private float maxLevelOfAcceptancy = 0.1f;

    [Header("Parameter  :   Animator")]
    public TransformAnimatorAsset appearAnimator;
    public TransformAnimatorAsset disappearAnimator;

    private bool _isFollowing;
    private bool _isAppeared;
    private float _indicatorDirection = 1f;

    private Vector2 _sizeOfIndicatorHolder;

    #endregion

    #region Configuretion

    private IEnumerator ControllerForFollowingTheObject() {

        Transform transformReferene = transform;
        WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

        while (_isFollowing) {

            parentOfIndicator.transform.position = transformReferene.position + positionOffset; 
            yield return waitForEndOfFrame;
        }

        StopCoroutine(ControllerForFollowingTheObject());
    }

    

    #endregion

    #region Public Callback

    public void PreProcess(UnityAction<float> OnPassingTheResult) {

        OnPassingTheResultEvent += OnPassingTheResult;

        _isAppeared = false;

        _indicatorDirection = 1f;
        InterpolatedPositionOfIndicator = 0;
        _sizeOfIndicatorHolder = parentOfIndicator.sizeDelta;
        Vector2Int sizeDelta = new Vector2Int(
            Mathf.Clamp((int)fillerImage.rectTransform.sizeDelta.x,256,512),
            Mathf.Clamp((int)fillerImage.rectTransform.sizeDelta.y,64,128)
        );
        Texture2D fillerTexture = new Texture2D(sizeDelta.x, sizeDelta.y);
        Color[] textureColors = new Color[sizeDelta.x * sizeDelta.y];

        int colorBlendStartPointForMinAccuracy = (int)((minLevelOfAcceptancy - (minLevelOfAcceptancy * colorBlendAreaOnLesserThanAccurate)) * sizeDelta.x);
        int colorBlendEndPointForMinAccuracy = (int)(minLevelOfAcceptancy * sizeDelta.x);
        int differenceBetweenPointsInMinAccuracy = colorBlendEndPointForMinAccuracy - colorBlendStartPointForMinAccuracy;

        int colorBlendStartPointForMaxAccuracy = (int)(maxLevelOfAcceptancy * sizeDelta.x);
        int colorBlendEndPointForMaxAccuracy = (int)((maxLevelOfAcceptancy + ((1f - maxLevelOfAcceptancy) * colorBlendAreaOnGreaterThanAccurate)) * sizeDelta.x);
        int differenceBetweenPointsInMaxAccuracy = colorBlendEndPointForMaxAccuracy - colorBlendStartPointForMaxAccuracy;


        for (int y = 0; y < sizeDelta.y; y++)
        {

            for (int x = 0; x < sizeDelta.x; x++)
            {

                int pixelIndex = (y * sizeDelta.x) + x;

                if (x <= colorBlendStartPointForMinAccuracy)
                {
                    textureColors[pixelIndex] = colorLesserThanAccurate;
                }
                else if (x > colorBlendStartPointForMinAccuracy && x < colorBlendEndPointForMinAccuracy)
                {
                    float blendValue = (x - colorBlendStartPointForMinAccuracy) / (float)differenceBetweenPointsInMinAccuracy;
                    textureColors[pixelIndex] = Color.Lerp(
                            colorLesserThanAccurate,
                            colorForAccurate,
                            blendValue
                        );
                }
                else if (x >= colorBlendEndPointForMinAccuracy && x <= colorBlendStartPointForMaxAccuracy)
                {
                    textureColors[pixelIndex] = colorForAccurate;
                }
                else if (x > colorBlendStartPointForMaxAccuracy && x < colorBlendEndPointForMaxAccuracy)
                {

                    float blendValue = (x - colorBlendStartPointForMaxAccuracy) / (float)differenceBetweenPointsInMaxAccuracy;
                    textureColors[pixelIndex] = Color.Lerp(
                            colorForAccurate,
                            colorGreaterThanAccurate,
                            blendValue
                        );
                }
                else
                {
                    textureColors[pixelIndex] = colorGreaterThanAccurate;
                }
            }
        }
        fillerTexture.SetPixels(textureColors);
        fillerTexture.Apply();
        fillerImage.sprite = Sprite.Create(fillerTexture, new Rect(0, 0, sizeDelta.x, sizeDelta.y), new Vector2(0.5f, 0.5f));

        switch (indicatorResponse)
        {
            case IndicatorResponse.Auto:
                Appear();
                break;
        }

       
    }

    public void PostProcess()
    {
        Disappear();
    }

    public void Appear()
    {
        if (!_isAppeared)
        {

            appearAnimator.DoAnimate(
            new List<Transform>() { parentOfPerformanceBar },
            () => {

                appearAnimator.DoAnimate(
                   new List<Transform>() { indicator },
                   () =>
                   {
                       switch (indicatorResponse)
                       {
                           case IndicatorResponse.Auto:
                               BatchedUpdate.Instance.RegisterToBatchedUpdate(this, 1);
                               break;
                       }
                       
                   });
            });

            _isFollowing = true;
            StartCoroutine(ControllerForFollowingTheObject());

            _isAppeared = true;
        }
    }

    public void Disappear() {

        if (_isAppeared) {

            switch (indicatorResponse)
            {
                case IndicatorResponse.Auto:
                    BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);
                    break;
            }

            disappearAnimator.DoAnimate(
                new List<Transform>() { indicator },
                () =>
                {
                    disappearAnimator.DoAnimate(
                        new List<Transform>() { parentOfPerformanceBar },
                        ()=> {
                            _isFollowing = false;
                        });
                }
            );

            _isAppeared = false;
        }
        
    }

    public void MoveIndicator(float interpolatedValue = -1) {

        Appear();

        if (interpolatedValue == -1)
        {
            InterpolatedPositionOfIndicator += Time.deltaTime * indicatorVelocity * _indicatorDirection;
        }
        else {
            InterpolatedPositionOfIndicator = interpolatedValue;
        }
        
        InterpolatedPositionOfIndicator = Mathf.Clamp01(InterpolatedPositionOfIndicator);

        Vector2 newIndicatorPosition = new Vector2(
                -(_sizeOfIndicatorHolder.x / 2.0f) + (InterpolatedPositionOfIndicator * _sizeOfIndicatorHolder.x),
                0f
            );
        indicator.anchoredPosition = newIndicatorPosition;

        switch (indicatorMode)
        {
            case IndicatorMode.OneGo:

                if (InterpolatedPositionOfIndicator >= 1)
                    OnStopingIndicator();

                break;

            case IndicatorMode.Rubberbend:

                if (_indicatorDirection > 0 && InterpolatedPositionOfIndicator >= 1)
                    _indicatorDirection = -1;
                else if (_indicatorDirection < 0 && InterpolatedPositionOfIndicator <= 0)
                    _indicatorDirection = 1;

                break;
        }
    }

    public void OnStopingIndicator()
    {

        float result = 0;
        if (InterpolatedPositionOfIndicator <= minLevelOfAcceptancy)
        {
            result = Mathf.Lerp(-1, 0, InterpolatedPositionOfIndicator / minLevelOfAcceptancy);
        }
        else if (InterpolatedPositionOfIndicator > minLevelOfAcceptancy && InterpolatedPositionOfIndicator < maxLevelOfAcceptancy)
        {
            result = Mathf.Lerp(
                    0,
                    1,
                    (InterpolatedPositionOfIndicator - minLevelOfAcceptancy) / (maxLevelOfAcceptancy - minLevelOfAcceptancy)
                );
        }
        else if (InterpolatedPositionOfIndicator > maxLevelOfAcceptancy)
        {

            result = Mathf.Lerp(
                1,
                2,
                (InterpolatedPositionOfIndicator - maxLevelOfAcceptancy) / (1f - maxLevelOfAcceptancy));
        }

        OnPassingTheResultEvent?.Invoke(result);

        BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);

    }

    public void OnBatchedUpdate()
    {
        MoveIndicator();
    }

    #endregion
}
