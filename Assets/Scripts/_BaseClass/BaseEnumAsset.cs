﻿using UnityEngine;

public abstract class BaseEnumAsset : ScriptableObject
{
    #region Public Access

    public string NameOfEnum { get { return nameOfEnum; } }
    public Sprite IconForEnum { get { return iconForEnum; } }

    #endregion

    #region SerializedField

    [SerializeField] private string nameOfEnum;
    [SerializeField] private Sprite iconForEnum;

    #endregion


}
